const body = document.body;
const numbers = document.querySelectorAll(".number");
const active = document.querySelectorAll("span");

active.forEach((el) => el.classList.add("hidden"));

numbers.forEach((el) => {
  el.addEventListener("click", function (e) {
    const target = e.target;
    const bgCol = window.getComputedStyle(target).backgroundColor;
    body.style.backgroundColor = bgCol;
    const active = target.firstElementChild;

    numbers.forEach((el) => {
      el.firstElementChild.classList.add("hidden");
    });
    active.classList.remove("hidden");
  });
});
